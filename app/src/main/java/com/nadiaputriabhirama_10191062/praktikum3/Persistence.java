package com.nadiaputriabhirama_10191062.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Persistence extends AppCompatActivity {

    EditText input;
    Button save, displayButton;
    TextView output;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);

        input = findViewById(R.id.input);
        save = findViewById(R.id.save);
        output = findViewById(R.id.output);
        displayButton = findViewById(R.id.displayButton);

        sharedPreferences = getSharedPreferences("latihanpersistance", MODE_PRIVATE);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(Persistence.this, "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }
        });

        displayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Persistence.this, Display.class));
            }
        });
    }

    public void getData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("data_saya",input.getText().toString() );
        editor.apply();
        output.setText("Output Data: " + sharedPreferences.getString("data_saya", null));


    }
}